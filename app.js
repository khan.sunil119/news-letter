//jshint esversion: 6
const express = require("express");
const request = require("request");
const https = require('https');

const app = express();
app.use(express.urlencoded({extended: true}));
app.use(express.static("public"));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/signup.html');
});

app.post('/', (req, res) => {
    var firstName = req.body.fName;
    var lastName = req.body.lName;
    var email = req.body.email;

    const data = {
        members: [
        {
            email_address: email,
            status: 'subscribed',
            merge_fields: {
                FNAME: firstName,
                LNAME: lastName
            }
        }
        ]
    };

    const jsonData = JSON.stringify(data);
    const url = "https://us6.api.mailchimp.com/3.0/lists/6d6a46ea2a";
    const options = {
        method: "POST",
        auth: "sunil19:c6319925b0a52d816eb315760395d763-us6"
    };
    const request = https.request(url, options, function(response){

        if (response.statusCode == 200) {
            res.sendFile(__dirname + '/success.html');
        }
        else{
            res.sendFile(__dirname + '/failure.html');  
        }

    });

    request.write(jsonData);
    request.end();
});

app.post('/failure', function(req, res){
    res.redirect('/');
});

app.listen(process.env.PORT || 3000,function(){
    console.log('Server is running on port 3000');
});